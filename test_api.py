from unittest.mock import patch, MagicMock
import unittest
import sys


class TestApi(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_decorator(self):
        from api import index
        assert index() == "Server Works!"
