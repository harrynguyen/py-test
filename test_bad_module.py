from unittest import TestCase
from unittest.mock import patch, MagicMock


class TestGoodMyModule(TestCase):
    """
    Let's test my module properly
    """

    def setUp(self):
        """
        It's patching time
        """

        # http://www.voidspace.org.uk/python/mock/examples.html#mocking-imports-with-patch-dict
        self.internetz_mock = MagicMock()
        self.internetz_mock.the.internetz.everything.log.return_value = True
        modules = {
            'the': self.internetz_mock,
            'the.internetz': self.internetz_mock.internetz,
        }

        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

    def tearDown(self):
        """
        Let's clean up
        """

        self.module_patcher.stop()

    def test_hello_should_say_waitforit_hello(self):
        """
        We want to test that the hello method calls the say function with the string "hello:
        """
        from my_module import MyModule
        m = MyModule()
        m.world = MagicMock()
        m.hello()

        m.world.say.assert_called_once_with("hello")
