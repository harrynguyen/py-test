from third_party_module import World


class MyModule(object):
    """
    My shining class
    """

    def __init__(self):
        """
        Let's instanciate the World (muhaha)
        """

        self.world = World()

    def hello(self):
        """
        My shining method
        """

        self.world.say("hello")
