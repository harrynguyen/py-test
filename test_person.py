from unittest.mock import patch, Mock
import unittest
import sys

sys.modules['decorators'] = Mock()
patch('decorators.noise_logger', lambda x: x).start()
from person import Person


class TestPerson(unittest.TestCase):
    def test_decorator(self):
        person = Person()
        assert person.pet.noise() == "Woof"
